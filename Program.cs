﻿using System;

namespace exercise_16
{
    class Program
    {
        static void Main(string[] args)
        {
         Console.WriteLine($"*******************************");
         Console.WriteLine($"****  Welcome To Ians Shop ****");
         Console.WriteLine($"*******************************");
         Console.WriteLine($"*******************************");
         Console.WriteLine($"       What is your name?      ");

         var myname = (Console.ReadLine());
         Console.WriteLine($"Your name is: {myname}");
        }
    }
}